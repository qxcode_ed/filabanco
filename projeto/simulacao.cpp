#include "simulacao.h"

int rand_min_max(int min, int max){
    static int init = 1;
    if(init){
        init = 0;
        srand(time(NULL));
    }
    return rand() % (max - min + 1) + min;
}

void simulacao(){
    Pintor pintor;
    int min_pac = 1, max_pac = 10;
    int min_doc = 1, max_doc = 5;



    // Tempo da simulação
    const unsigned int tempo = TURNOS_DIA;
    const unsigned int numCaixas = 3;

    // Estruturas para representar os clientes
    // no caixa e na fila
    std::vector<Cliente*> caixa(numCaixas);
    std::list<Cliente*> fila;

    int qtd = 5;
    for(int i = 0; i < qtd; i++)
        fila.push_back(new Cliente(rand_min_max(min_pac, max_pac),
                                   rand_min_max(min_doc, max_doc)));

    caixa[1] = new Cliente(MAX_PACIENCIA/3, 5);

    for (unsigned int i = 0; i < tempo; i++)
    {
        //desenha e espera 300 milisegundos
        Cliente *c = caixa[1];
        c->paciencia = rand() % MAX_PACIENCIA + 1;
        pintor.desenhar(caixa, fila, 300);
    }

    delete caixa[1];
    caixa[1] = nullptr;
    for(int i = 1; i < qtd; i++){
        delete fila.front();
        fila.pop_front();
    }

}


int otimizar_lucro(int cli_min, int cli_max, int pac_min, int pac_max, int doc_min, int doc_max){

    int vezes = N_EXEC_TESTE;
    std::vector<int> saldo(MAX_CAIXAS, 0);
    for(int num_caixas = 1; num_caixas <= MAX_CAIXAS; num_caixas++){
        for(int iv = 0; iv < vezes; iv++)
        {
            saldo[num_caixas - 1] += calcular_lucro(
                        cli_min, cli_max,
                        pac_min, pac_max,
                        doc_min, doc_max,
                        num_caixas);
        }
    }

    for(auto &s : saldo)
        s = s / vezes;

    for(auto &s : saldo)
        std::cout << s << " ";
    std::cout << std::endl;

    int pos_max = 0;
    int val_max = saldo[0];
    for(int i = 0; i < MAX_CAIXAS; i++){
        if(saldo[i] > val_max){
            pos_max = i;
            val_max = saldo[i];
        }
    }
    std::cout << "Val max " << val_max << " Caixa ideal " << pos_max + 1     << " \n";
    return pos_max + 1;
}


int calcular_lucro(int cli_min, int cli_max, int pac_min, int pac_max, int doc_min, int doc_max, int num_caixas){

    int docsProcess = 0;
    std::vector<Cliente*> caixa(num_caixas,0);
    std::list<Cliente*> fila;

    for  (int t = 0; t < TURNOS_DIA; t++)
    {
        // Inserindo novas pessoas na fila
        int pessoasChegando = rand_min_max(cli_min, cli_max);

        for (int j = 0; j < pessoasChegando; j++)
            fila.push_back(new Cliente(rand_min_max(pac_min, pac_max),
                                       rand_min_max(doc_min, doc_max)));

        // Gerenciando fila

        std::list<Cliente*>::iterator it;
        for (it = fila.begin(); it != fila.end();){
            if ((*it)->paciencia <= 0){
                delete *it;
                //retira e retorna a referencia do proximo
                it = fila.erase(it);
            }else{
                (*it)->paciencia--;
                it++;
            }
        }

        // Gerenciando o caixa
        for (int ik = 0; ik < int(caixa.size()); ik++)
        {
            //caixa vazio
            if (caixa[ik] == nullptr){
                //fila tem gente
                if(!fila.empty()){
                    caixa[ik] = fila.front();
                    fila.pop_front();
                }
            }else{
                //se terminou, dispacha o cliente
                if (caixa[ik]->documentos == 0){
                    delete caixa[ik];
                    caixa[ik] = nullptr;
                }else{
                    caixa[ik]->documentos--;
                    docsProcess++;
                }
            }
        }
    }//terminados a simulacao

    //removendo os clientes que nao foram processados nos turnos do dia

    for (auto &c : caixa){
        delete c;
        c = nullptr;
    }

    for (auto c : fila)
    {
        delete c;
        c = nullptr;
    }

    return (docsProcess - num_caixas * CUSTO_CAIXA_DIA);
}

void executar_testes(){
    //funcao auxiliar
    auto testar = [](bool resultado){
        cout << (resultado? "Passou!" : "Falhou!") << endl;
    };
    /*
    int calcularQtdOtimaCaixas(int cli_min, int cli_max,
                               int pac_min, int pac_max,
                               int doc_min, int doc_max);
    */

    cout << "Simulacao 1: ";
    testar(otimizar_lucro(0, 1, 1, 60, 1, 10) == 4);

    cout << "Simulacao 2: ";
    testar(otimizar_lucro(0, 2, 1, 20, 1, 10) == 8);

    cout << "Simulacao 3: ";
    testar(otimizar_lucro(0, 5, 1, 25, 1, 10) == 10);

    cout << "Simulacao 4: ";
    testar(otimizar_lucro(0, 4, 1, 5, 1, 2) == 6);

    cout << "Simulacao 5: ";
    testar(otimizar_lucro(1, 2, 1, 15, 1, 2) == 5);
}
